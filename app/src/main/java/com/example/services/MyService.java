package com.example.services;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.nfc.Tag;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class MyService extends Service implements SensorEventListener {
    SensorManager sensorManager;
    List<Coordinate> coordinates = new ArrayList<>();
    Disposable subscribe;


    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();

        subscribe = Observable.interval(1, TimeUnit.MINUTES)
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
                        sensorManager.registerListener(MyService.this,
                                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                                SensorManager.SENSOR_DELAY_NORMAL);

                    }
                }, throwable -> Toast.makeText(MyService.this, "Service is Dead", Toast.LENGTH_LONG).show());


//        return START_NOT_STICKY;  //Не восстановит работу
        return START_STICKY; //Восстановит сервис когда будет достаточно ресурсов

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            coordinates.add(new Coordinate(event.values[0], event.values[1], event.values[2]));

            int max = coordinates.size()-1;

            if (coordinates.size() >= 10) {
                MyDataBase myDataBase = MyDataBase.getDataBase(this);

                myDataBase.getDao().insertAll(coordinates);

//                Toast.makeText(this, "Hi from service", Toast.LENGTH_LONG).show();
                Toast.makeText(this, coordinates.get(max).getMovement(), Toast.LENGTH_LONG).show();

                Log.d("from Service", "hi from service");

                sensorManager.unregisterListener(this);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    @Override
    public void onDestroy() {
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();

        subscribe.dispose();

        super.onDestroy();
    }



}
