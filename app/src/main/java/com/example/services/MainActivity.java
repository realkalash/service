package com.example.services;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    Button actionStart;
    Button actionStopped;
    EditText editText;

    TextView txtSum;
    TextView txtLazy;
    TextView txtActive;
    TextView txtErrors;


    View greenIndicator, redIndicator;
    LinearLayout.LayoutParams redParams;
    LinearLayout.LayoutParams greenParams;
    LinearLayout.LayoutParams indicatorsParams;
    LinearLayout am_lIndicators;


    Disposable subscribe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionStart = findViewById(R.id.startService);
        actionStopped = findViewById(R.id.stopService);
        editText = findViewById(R.id.editText);

        txtActive = findViewById(R.id.am_activeTime);
        txtSum = findViewById(R.id.am_sum);
        txtLazy = findViewById(R.id.am_lazyTime);
        txtErrors = findViewById(R.id.am_errors);

        am_lIndicators = findViewById(R.id.am_lIndicators);

        greenIndicator = findViewById(R.id.am_green);
        redIndicator = findViewById(R.id.am_red);

        redParams = (LinearLayout.LayoutParams) redIndicator.getLayoutParams();
        greenParams = (LinearLayout.LayoutParams) greenIndicator.getLayoutParams();
        indicatorsParams = (LinearLayout.LayoutParams) am_lIndicators.getLayoutParams();


        MyDataBase myDataBase = MyDataBase.getDataBase(this);

        Intent intentService = new Intent(MainActivity.this, MyService.class);

        counterActivity(myDataBase.getDao().selectAll());


//        Intent intentServiceMain = new Intent(MainActivity.this, MyService.class);
//        startService(intentServiceMain);

        subscribe = Observable.interval(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {

                        counterActivity(myDataBase.getDao().selectAll());


                    }
                }, throwable -> Toast.makeText(MainActivity.this, "error with DataBase", Toast.LENGTH_SHORT).show());

        actionStart.setOnClickListener(v -> {

            startService(intentService);

        });

        actionStopped.setOnClickListener(v -> {

            myDataBase.getDao().removeAll();

            stopService(intentService);
        });
    }

    private void counterActivity(List<Coordinate> coordinates) {
        int sum = coordinates.size();
        int lazy = 0;
        int active = 0;
        int errors = 0;

        for (Coordinate coordinate : coordinates) {

            String typeMove = coordinate.getMovement();

            if (typeMove.equals(Coordinate.lazy)) {
                active++;
            }
            if (typeMove.equals(Coordinate.walk)
                    || typeMove.equals(Coordinate.fast_walk)
                    || typeMove.equals(Coordinate.run)) {
                lazy++;
            }
            if (typeMove.equals(Coordinate.error)) {
                errors++;
            }
        }

        View gre = greenIndicator.findViewById(R.id.am_green);

        gre.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                Math.abs((float) active)));

        View red = redIndicator.findViewById(R.id.am_red);

        red.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                (Math.abs((float) lazy))));

        txtLazy.setText(String.format("Lazy time: %d min", active));
        txtActive.setText(String.format("Active time: %d min", lazy));
        txtSum.setText(String.format("Sum time: %d min", sum));
        txtErrors.setText(String.format("Errors count: %d min", errors));
    }

    @Override
    protected void onDestroy() {
//        Intent intent = new Intent(MainActivity.this, MyService.class);
//        stopService(intent);
        super.onDestroy();

    }

    @Override
    protected void onStop() {
//        Intent intent = new Intent(MainActivity.this, MyService.class);
//        stopService(intent);
        subscribe.dispose();
        super.onStop();
    }
}
