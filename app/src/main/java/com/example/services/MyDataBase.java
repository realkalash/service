package com.example.services;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Coordinate.class}, version = 6)
public abstract class MyDataBase extends RoomDatabase {

    private static MyDataBase INSTANCE;

    public static MyDataBase getDataBase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), MyDataBase.class, "database")
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();
            return INSTANCE;
        } else return INSTANCE;
    }
    public static void DestroyDatabase(){
        INSTANCE = null;
    }

    public abstract MyDataBaseDao getDao();

}
