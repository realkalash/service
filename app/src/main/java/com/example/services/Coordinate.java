package com.example.services;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Coordinate {
    public static final String lazy = "спокойствие";
    public static final String error = "error";
    public static final String walk = "ходьба";
    public static final String fast_walk = "быстрая ходьба";
    public static final String run = "бег";

    @PrimaryKey(autoGenerate = true)
    int id;
    private String movement;

    private float ax;
    private float ay;
    private float az;

    public Coordinate(float ax, float ay, float az) {
        this.ax = ax;
        this.ay = ay;
        this.az = az;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMovement() {

        // квадрат модуля ускорения телефона, деленный на квадрат
        //ускорения свободного падения
//        float accelationSquareRoot = (float) Math.sqrt(((ax * ax) + (ay * ay) + (az * az)));
        float accelationSquareRoot = (float) ((ax * ax + ay * ay + az * az) / (9.8 * 9.8));

        if (accelationSquareRoot < 1) {
            return error;
        } else if (accelationSquareRoot >= 1 && accelationSquareRoot < 1.2) {
            return lazy;
        } else if (accelationSquareRoot >= 1.2 && accelationSquareRoot <= 1.59) {
            return walk;
        } else if (accelationSquareRoot > 1.59 && accelationSquareRoot <= 2) {
            return fast_walk;
        } else {
            return run;
        }

        /*<1 error*/
        /*1.2 walk*/
        /*2 fast walk*/
        /*2> run*/
    }

    public void setMovement(String movement) {
        this.movement = movement;
    }

    public float getAx() {
        return ax;
    }

    public void setAx(float ax) {
        this.ax = ax;
    }

    public float getAy() {
        return ay;
    }

    public void setAy(float ay) {
        this.ay = ay;
    }

    public float getAz() {
        return az;
    }

    public void setAz(float az) {
        this.az = az;
    }
}
