package com.example.services;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public abstract class MyDataBaseDao {


    @Insert
    public abstract void insertAll(List<Coordinate> filmsModels);

    @Query("SELECT * FROM Coordinate")
    // позволяет использовать буферизацию. Когда выдираем большое кол-во данных, можно выдирать только по чуть-чуть к примеру обьектов
    public abstract List<Coordinate> selectAll();                     // тот же обзёрв

    @Query("DELETE FROM Coordinate")
    public abstract void removeAll();
}
